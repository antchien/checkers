# -*- coding: utf-8 -*-
require 'colorize'
require_relative 'board'
require_relative 'piece'

class InvalidMoveError < ArgumentError; end

class Game
  attr_reader :board, :turn

  def initialize
    @board = Board.new
    @turn = [:white, :black]
    play
  end

  def play

    while !board.game_over?
      board.display_board
      puts "It is #{turn.first.to_s}'s turn. Please choose a move: "

      begin
        user_input = gets.chomp.split(" ")
        move_sequence = translate_pos(user_input)
        board.check_turn(move_sequence, turn)
        board.perform_moves(move_sequence)
      rescue InvalidMoveError => e
        puts e.message
        puts "Please enter another move: "
        retry
      end
      board.promote_any_kings
      turn.reverse!
    end

    puts "Game Over! #{winner.to_s.upcase} team wins!!"
  end

  def translate_pos(raw_pos_arr)
    trans_pos = []
    raw_pos_arr.each do |pos|
      trans_pos << [(pos[1].to_i-8).abs, "abcdefgh".index(pos[0])]
    end

    trans_pos
  end

end





