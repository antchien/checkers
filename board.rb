# -*- coding: utf-8 -*-
require_relative 'piece'

class Board
  attr_accessor :grid, :pieces

  def initialize(fill_board = true)
    @grid = Array.new(8) { Array.new(8) }
    @pieces = []
    set_init_pieces if fill_board
  end

  def [](pos)
    x, y = pos
    @grid[x][y]
  end

  def []=(pos, value)
    x, y = pos
    @grid[x][y] = value
  end

  def set_init_pieces
    #set black pieces
    (0..2).each do |x|
      (0..7).each do |y|
        add_piece([x,y], :black, false, self) if x != 1 && y % 2 == 1
        add_piece([x,y], :black, false, self) if x == 1 && y % 2 == 0
      end
    end

    #set red pieces
    (5..7).each do |x|
      (0..7).each do |y|
        add_piece([x,y], :white, false, self) if x == 6 && y % 2 == 1
        add_piece([x,y], :white, false, self) if x != 6 && y % 2 == 0
      end
    end
  end

  def check_turn(move_sequence, turn)
    if self[move_sequence.first].nil?
      raise InvalidMoveError, "Initial space is nil!"
    end

    if self[move_sequence.first].color != turn.first
      raise InvalidMoveError, "It is #{turn.first}'s turn!  You tried to move #{self[move_sequence.first].color}'s piece, cheater."
    end
  end

  def translate_pos(raw_pos_arr)
    trans_pos = []
    raw_pos_arr.each do |pos|
      trans_pos << [(pos[1].to_i-8).abs, "abcdefgh".index(pos[0])]
    end

    trans_pos
  end

  def winner
    return :white if !any_black_left?
    return :black if !any_white_left?
  end

  def game_over?
    !any_black_left? || !any_white_left?
  end

  def any_black_left?
    pieces.any? { |pieces| pieces.color == :black }
  end

  def any_white_left?
    pieces.any? { |pieces| pieces.color == :white }
  end

  def add_piece(pos, color, is_king, board)
    new_piece = Piece.new(pos, color, is_king, board)
    self[pos] = new_piece
    pieces << new_piece
  end

  def remove_piece(pos)
    if !self[pos].nil?
      pieces.delete(self[pos])
      self[pos] = nil
    else
      raise InvalidMoveError, "The space you entered to delete is nil!"
    end
  end

  def any_jump_avail?(color)
    pieces.any? { |piece| piece.color == color && !piece.jump_moves.empty? }
  end

  def on_board?(pos)
    (0..7).include?(pos[0]) && (0..7).include?(pos[1])
  end

  def square_empty?(pos)
    self[pos].nil?
  end

  def occupied_by_opp?(pos, own_color)
    !self[pos].nil? && own_color != self[pos].color
  end

  def perform_slide(start_pos, end_pos)
    if !self[start_pos].nil? && self[start_pos].slide_moves.include?(end_pos)
      raise InvalidMoveError, "You can not slide if there is a jump move available!" if any_jump_avail?(self[start_pos].color)
      self[start_pos], self[end_pos] = self[end_pos], self[start_pos]
      self[end_pos].position = end_pos
    else
      raise InvalidMoveError, "The slide move you entered is invalid!"
    end
    nil
  end

  def perform_jump(start_pos, end_pos)
    if !self[start_pos].nil? && self[start_pos].jump_moves.include?(end_pos)
      self[start_pos], self[end_pos] = self[end_pos], self[start_pos]
      self[end_pos].position = end_pos
      victim_pos = [(start_pos[0] + end_pos[0]) / 2, (start_pos[1] + end_pos[1]) / 2]
      remove_piece(victim_pos)
    else
      raise InvalidMoveError, "The jump move you entered is invalid!"
    end
    nil
  end

  def perform_moves(move_sequence)
    if valid_move_seq?(move_sequence)
      perform_moves!(move_sequence)
    else
      raise InvalidMoveError, "This is an Invalid move sequence!!"
    end
  end

  def perform_moves!(move_sequence)
    case move_sequence.length
    when 0..1
      raise InvalidMoveError, "Not enough moves!" if move_sequence.length
    when 2
      is_jump_sequence = false
    else
      is_jump_sequence = true
    end
    (0...move_sequence.length-1).each do |pos_index|
      perform_move!( move_sequence[pos_index], move_sequence[pos_index+1], is_jump_sequence)
    end
  end

  def perform_move!(start_pos, end_pos, is_jump_sequence = false)
    moved = false
    raise InvalidMoveError, "Start position is nil!" if self[start_pos].nil?
    moving_piece = self[start_pos]

    if moving_piece.slide_moves.include?(end_pos) && !is_jump_sequence
      perform_slide(start_pos, end_pos)
      moved = true
    end
    if moving_piece.jump_moves.include?(end_pos)
      perform_jump(start_pos, end_pos)
      moved = true
    end
    raise InvalidMoveError, "Invalid Move trying to go from #{start_pos} to #{end_pos}" if moved == false
  end

  def dup
    new_board = Board.new(false)
    pieces.each do |piece|
      new_board.add_piece(piece.position.dup,
                          piece.color,
                          piece.is_king,
                          new_board)
    end

    new_board
  end

  def valid_move_seq?(move_sequence)
    new_board = self.dup
    begin
      new_board.perform_moves!(move_sequence)
    rescue
      return false
    end

    true
  end

  def promote_any_kings
    (0..7).each do |index|
      if !self[[0,index]].nil? && self[[0,index]].color == :white
        self[[0,index]].is_king = true
        puts "King promotion!"
      elsif !self[[7,index]].nil? && self[[7,index]].color == :black
        self[[0,index]].is_king = true
        puts "King promotion!"
      end
    end
  end

  def display_board
    sq_color_index = 1
    @grid.each_with_index do |row, x|
      print (8-x).abs
      row.each_with_index do |square, y|
        if sq_color_index % 2 == 0
          print "   ".colorize(:background => :light_black) if square.nil?
          print " #{square.get_symbol} ".colorize(:background => :light_black) unless square.nil?
        else
          print "   ".colorize(:background => :red) if square.nil?
          print " #{square.get_symbol} ".colorize(:background => :red) unless square.nil?
        end
        sq_color_index += 1
      end
      puts
      sq_color_index +=1
    end
    puts "  a  b  c  d  e  f  g  h "
  end

end