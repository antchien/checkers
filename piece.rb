# -*- coding: utf-8 -*-
require_relative 'board'

class Piece
  attr_accessor :position, :color, :is_king, :board

  def initialize(pos, color, is_king, board)
    @position = pos
    @color = color
    @is_king = is_king
    @board = board
  end

  def get_symbol
    return "♙" if color == :white && !is_king
    return "♔" if color == :white && is_king
    return "♟" if color == :black && !is_king
    return "♚" if color == :black && is_king
  end

  def slide_moves
    possible_moves = []
    possible_deltas = get_move_deltas(1)

    possible_deltas.each do |delta|
      move = [position[0] + delta[0], position[1] + delta[1]]
      possible_moves << move if board.on_board?(move) && board.square_empty?(move)
    end

    possible_moves
  end

  def jump_moves
    possible_moves = []
    possible_deltas = get_move_deltas(1)
    possible_deltas.each do |delta|
      jumped_pos = [position[0] + delta[0], position[1] + delta[1]]
      if !board.square_empty?(jumped_pos) && board.occupied_by_opp?(jumped_pos, color)
        move = [jumped_pos[0] + delta[0], jumped_pos[1] + delta[1]]
        possible_moves << move if board.on_board?(move) && board.square_empty?(move)
      end
    end

    possible_moves
  end

  def get_move_deltas(x)
    return [[x,x], [x,-x], [-x, x], [-x, -x]] if is_king
    return [[-x,x], [-x,-x]] if color == :white && !is_king
    return [[x,x], [x,-x]] if color == :black && !is_king

  end

end
